package fun.barryhome.ddd.domain.enums;

/**
 * Created on 2020/9/7 4:47 下午
 *
 * @author barry
 * Description:
 */
public enum TradeType {
    /**
     * 充值
     */
    RECHARGE,
    /**
     * 消费
     */
    CONSUME,
    /**
     * 转账
     */
    TRANSFER,
    /**
     * 锁定
     */
    LOCK,
    /**
     * 充值退款
     */
    RECHARGE_ROLLBACK,
}
