package fun.barryhome.ddd.domain.enums;

/**
 * Created by heyong on 2017/2/17 15:19.
 */
public enum InOutFlag {
    /**
     * 其它
     */
    NONE,
    /**
     * 进
     */
    IN,
    /**
     * 出
     */
    OUT,
}